//
//  BasketballRowController.swift
//  RaptorsBasketballApp WatchKit Extension
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit

class BasketballRowController: NSObject {

    // Outlets for the table row
    @IBOutlet weak var team1Label: WKInterfaceLabel!
    @IBOutlet weak var team2Label: WKInterfaceLabel!
    @IBOutlet weak var locationLabel: WKInterfaceLabel!
    @IBOutlet weak var startTimeLabel: WKInterfaceLabel!
}
