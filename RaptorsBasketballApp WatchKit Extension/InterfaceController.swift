//
//  InterfaceController.swift
//  RaptorsBasketballApp WatchKit Extension
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet weak var gameTable: WKInterfaceTable!
    
    //MARK: Data source
    var gameList:[BasketballGameObject] = []
    
    func createGameObjects() {
        let d = Date()
        let g1 = BasketballGameObject(teamAName: "Trail Blazers", teamBName: "Raptors", gameLocation: "Toronto", startTime: d)
        let g2 = BasketballGameObject(teamAName: "Raptors", teamBName: "Pistons", gameLocation: "Detroit", startTime: d)
        let g3 = BasketballGameObject(teamAName: "Lakers", teamBName: "Raptors", gameLocation: "Toronto", startTime: d)
        let g4 = BasketballGameObject(teamAName: "Raptors", teamBName: "Knicks", gameLocation: "New York", startTime: d)
        
        
        gameList.append(g1)
        gameList.append(g2)
        gameList.append(g3)
        gameList.append(g4)
        
    }
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
        // 1. create the games
        self.createGameObjects()
        
        
        // 2. populate the table
        // 2a. tell ios how many rows are in the table
        // 2b. put the data into the row
        // ---------
        self.gameTable.setNumberOfRows(self.gameList.count, withRowType:"myRow")
        
        // 1. loop through your array
        // 2. take each item in the array and put it in a table row
        for (i, g) in self.gameList.enumerated() {
            let row = self.gameTable.rowController(at: i) as! BasketballRowController

            row.team1Label.setText(g.teamAName!)
            row.team2Label.setText(g.teamBName!)
            row.locationLabel.setText(g.gameLocation!)
            
            
            // converting Date() objects to strings
            // so we can put the start time into the label
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm"
            let dateString = dateFormatter.string(from:g.startTime!)
            
            row.startTimeLabel.setText(dateString)
        }
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
