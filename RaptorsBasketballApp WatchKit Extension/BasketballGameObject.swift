//
//  BasketballGameObject.swift
//  RaptorsBasketballApp WatchKit Extension
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit

class BasketballGameObject: NSObject {
    
    // MARK: class properties
    var teamAName:String?
    var teamBName:String?
    var gameLocation:String?
    var startTime:Date?
    
    // MARK: contructor
    convenience override init() {
        let d = Date()
        self.init(teamAName:"", teamBName:"", gameLocation:"", startTime:d)
    }
    
    init(teamAName:String, teamBName:String, gameLocation:String, startTime:Date) {
        
        self.teamAName = teamAName
        self.teamBName = teamBName
        self.gameLocation = gameLocation
        self.startTime = startTime
        
        
    }
    
}
